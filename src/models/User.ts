import { Entity, ObjectID, ObjectIdColumn, Column, BaseEntity } from 'typeorm';
import bcrypt from 'bcrypt';
@Entity()
export class User extends BaseEntity {

  @ObjectIdColumn()
  id: ObjectID;

  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @Column()
  email: string;

  @Column()
  hashPassword: string;

  comparePassword(password: string) {
    return bcrypt.compareSync(password, this.hashPassword);
  }

}