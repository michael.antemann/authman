import { createConnection, Connection } from 'typeorm';
import { User } from './models/User';
const production = process.env.NODE_ENV === 'production';


export const connectDB = async () => {
  return createConnection({
    type: 'mongodb',
    host: production ? 'mongo' : 'localhost',
    port: 27017,
    username: 'admin',
    password: 'D+N+GA4E5ncL=VLK',
    database: 'authman',
    entities: [User]
  });
};
