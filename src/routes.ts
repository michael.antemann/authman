import express from 'express';
export const router = express.Router();

import * as usersController from './controllers/users_controller';
import { UserRequest } from './interfaces/user-request';

router.post('/login', usersController.login);
router.post('/register', usersController.register);
router.delete('/:id', usersController.deleteById);

// router.use(usersController.validate);

router.get('/', usersController.validate, (req: UserRequest, res: express.Response) => {
  res.json({
    text: 'Hello World',
    user: req.user
  });
});