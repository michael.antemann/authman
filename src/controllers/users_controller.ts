import { Request, Response, NextFunction } from 'express';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';

import { User } from '../models/User';
import { UserRequest } from '../interfaces/user-request';

const JWT_SECRET = '3yf%]$U5~ZJ@W8zm';

export const register = async (req: Request, res: Response) => {
  const alreadyInUse = User.findOne({
    email: req.body.email
  });
  if (alreadyInUse) {
    return res.status(500).json({
      message: 'Email already in use.'
    });
  }
  const user = new User();
  user.firstName = req.body.firstName;
  user.lastName = req.body.lastName;
  user.email = req.body.email;
  user.hashPassword = bcrypt.hashSync(req.body.password, 10);
  await user.save();
  user.hashPassword = undefined;
  return res.json(user);
};

export const login = async (req: Request, res: Response) => {
  const user = await User.findOne({
    email: req.body.email
  });

  const authenticated = user && user.comparePassword(req.body.password);
  if (!authenticated) {
    return res.status(401).json({ message: 'Authentication failed.' });
  }

  return res.json({
    token: jwt.sign({
      id: user.id,
      email: user.email,
      fullName: user.firstName + ' ' + user.lastName
    }, JWT_SECRET)
  });
};

export const loginRequired = (req: any, res: Response, next: NextFunction) => {
  if (req.user) {
    next();
  } else {
    return res.status(401).json({ message: 'Unauthorized User' });
  }
};
/**
 * validation middleware
 * @param req.user contains user data
 */
export const validate = async (req: UserRequest, res: Response, next: NextFunction) => {
  const bearer = req.get('authorization');
  if (bearer && bearer.split(' ')[0] === 'Bearer') {
    jwt.verify(bearer.split(' ')[1], JWT_SECRET, async (err, payload: any) => {
      if (err) {
        return res.status(403).json({
          message: 'Unauthorized'
        });
      }
      req.user = await User.findOneById(payload.id);
      if (req.user) {
        next();
      } else {
        return res.status(403).json({
          message: 'Unauthorized'
        });
      }
    });
  } else {
    return res.status(403).json({
      message: 'Unauthorized'
    });
  }
};

export const deleteById = async (req: UserRequest, res: Response, next: NextFunction) => {
  const user = await User.removeById(req.params.id);
  console.log(user);
  return res.json({
    result: user
  });
};

export const getAll = async (req: UserRequest, res: Response, next: NextFunction) => {
  const users = await User.find();
  return res.json({
    data: users
  });
};