import express from 'express';
import bodyParser from 'body-parser';
import morgan from 'morgan';
import 'reflect-metadata';
import { router } from './routes';
import { connectDB } from './db';
const app = express();
app.use(bodyParser.json());
app.use(morgan('combined'));

app.use('/', router);

async function init() {
  const connection = await connectDB();
}

const PORT = 3000;
app.listen(PORT, () => {
  init();
});
