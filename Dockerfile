FROM node:8
WORKDIR /home/node/app
COPY . /home/node/app
RUN rm -f -R /home/node/app/node_modules
RUN npm install
CMD npm run start